//
//  ContentResponse.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation
import SwiftyJSON

class ContentJSON {
    var page: Int
    var pages: Int
    var perpage: Int
    var total: String
    var photo: [PhotoJSON] = []
    
    init(json: JSON){
        page = json["page"].int!
        pages = json["pages"].int!
        perpage = json["perpage"].int!
        total = json["total"].string!
        if let photos: [JSON] = json["photo"].array {
            for photoJSON in photos {
                self.photo.append(PhotoJSON(json: photoJSON))
            }
        }
    }
    
}

//
//  CollectionManager.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation
import SwiftyJSON

class CollectionManager: RequestDelegate  {
    
    // MARK: Properties
    
    var request: Request!
    var pageNumber: Int = 1
    var collectionItems: [CollectionItem] = []
    var collectionView: UICollectionView!
    var selectedItem: CollectionItem!
    
    init(collectionView: UICollectionView) {
        request = Request(delegate: self)
        request.doRequest(url: Url.generateRequestUrlForGet(DataOnNumberOfPage: pageNumber))
        self.collectionView = collectionView
    }
    
    // MARK: Rest Functions
    
    // Called when need download more images
    func needDownloadMoreImages(){
        request.doRequest(url: Url.generateRequestUrlForGet(DataOnNumberOfPage: pageNumber))
        pageNumber += 1
    }
    
    // On error ocurred perform rest request
    func onErrorOccurred(error: Int) {
        // TODO - DIALOG
    }
    
    // When response of request is received
    func onRequestResponse(data: Data) {
        let responseFlickr = FlickrJSON(json: JSON(data))
        collectionItems.append(contentsOf: generateURLForEachImage(response: responseFlickr))
        DispatchQueue.main.async(execute: { () -> Void in
            self.collectionView.reloadData()
        })
    }
    
    // Transforms FlickrJSON into array of CollectionItem
    func generateURLForEachImage(response: FlickrJSON) -> [CollectionItem] {
        var items: [CollectionItem] = []
        for photo in response.photos.photo{
            items.append(CollectionItem(smallImageUrl: Url.generateRequestUrlForGet(Photo: photo, size: .miniature), bigImageUrl: Url.generateRequestUrlForGet(Photo: photo, size: .big), enabled: true))
        }
        return items
    }
    
    // MARK: Collection Functions
    
    // Return the number of Sections in collectionView
    func getNumberOfSectionsInCollection() -> Int {
        return 1
    }
    
    // Return the number of Items in Section
    func getNumberOfItemsInCollection() -> Int {
        return collectionItems.count
    }
    
    // Return the CollectionItem corresponding with position of CollectionView
    func getItemAtPositionInCollection(row: Int) -> CollectionItem {
        return collectionItems[row]
    }
    
    // Return the small image url of CollectionItem at Position
    func getUrlAtPositionInCollection(row: Int) -> String {
        return collectionItems[row].smallImageUrl
    }
    
    // Return CollectionItem is enabled or not
    func isEnabledItemAtPosition(row: Int) -> Bool {
        return collectionItems[row].isEnabled
    }
    
    // Set current selected item in CollectionView
    func setSelectedItem(AtRow row: Int){
        self.selectedItem = collectionItems[row]
    }
    
    // Change the state of enabled property of CollectionView
    func changeState(forItemAtPosition row: Int, withState state: Bool){
        self.collectionItems[row].isEnabled = state
    }
    
}

//
//  Url.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation

class Url {
    
    public static func generateRequestUrlForGet(Photo photo: PhotoJSON, size: SizesOfImage) -> String {
        return "https://farm" + String(photo.farm) + ".staticflickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + "_" + size.rawValue + ".jpg"
    }
    
    public static func generateRequestUrlForGet(DataOnNumberOfPage page: Int) -> String {
        return page == 0 ? simpleUrlForFetchData : completeUrlForFetchData + String(page)
    }
    
    private static let simpleUrlForFetchData: String = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=939af47c832dd92c423d68dd2c23e54b&tags=barcelona%20city&per_page=20&format=json&nojsoncallback=1"
    
    private static let completeUrlForFetchData: String = simpleUrlForFetchData + "&page="
    
    enum SizesOfImage: String {
        case square = "s"
        case large = "q"
        case miniature = "t"
        case small = "m"
        case small_240 = "n"
        case medium_500 = "-"
        case medium_640 = "z"
        case medium_800 = "c"
        case big = "b"
        case big_1600 = "h"
        case big_2048 = "k"
        case original = "o"
    }
   
}


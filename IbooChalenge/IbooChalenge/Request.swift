//
//  Request.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 12/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol RequestDelegate {
    func onRequestResponse(data: Data)
    func onErrorOccurred(error: Int)
}

class Request {
    
    var delegate: RequestDelegate!
    
    init(delegate: RequestDelegate){
        self.delegate = delegate
    }
    
    func doRequest(url: String){
        URLSession.shared.dataTask(with: URL(string: url)!) { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(error)")
                self.delegate.onErrorOccurred(error: 500)
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.delegate.onErrorOccurred(error: httpStatus.statusCode)
            }
            self.delegate.onRequestResponse(data: data!)
        }.resume()
    }
    
   
}

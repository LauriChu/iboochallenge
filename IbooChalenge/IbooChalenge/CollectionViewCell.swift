//
//  CollectionViewCell.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 11/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import UIKit
import SDWebImage

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var mSwitch: UISwitch!
    
    func inflate(image: String, isEnabled: Bool){
        CellManager.inflateSDWebImage(image: self.image, url: image)
        self.mSwitch.isOn = isEnabled
    }
}

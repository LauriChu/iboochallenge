//
//  CollectionViewController.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import UIKit
import SDWebImage

class CollectionViewController: UICollectionViewController {
    
    // MARK: Properties
    
    var manager: CollectionManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Creates Manager Object
        manager = CollectionManager(collectionView: self.collectionView!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.toDetail {
            let detailViewControlller: DetailViewController = segue.destination as! DetailViewController
            detailViewControlller.itemCollection = manager.selectedItem
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return manager.getNumberOfSectionsInCollection()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return manager.getNumberOfItemsInCollection()
    }
    
    // Inflate cell at IndexPath
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellManager.CellId.collection.rawValue, for: indexPath) as! CollectionViewCell
        let item = manager.getItemAtPositionInCollection(row: indexPath.row)
        cell.inflate(image: item.smallImageUrl, isEnabled: item.isEnabled)
        cell.mSwitch.tag = indexPath.row
        cell.mSwitch.addTarget(self, action: #selector(CollectionViewController.onSwitchChange(_:)), for: .valueChanged)
        return cell
    }
    
    func onSwitchChange(_ sender: UISwitch){
        manager.changeState(forItemAtPosition: sender.tag, withState: sender.isOn)
    }
    
    // MARK: Add Supplementary view for Footer Loading
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: CellManager.CellId.load.rawValue, for: indexPath)
        return cell
    }
    
    // MARK: Load more images
    // If user arrives at bottom of collectionView that will call will DisplaySupplementaryView(footer) and will need more images
    
    override func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        manager.needDownloadMoreImages()
    }
    
    // MARK: UICollectionViewDelegate
    
    // On select item navigates to DetailViewController
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        manager.setSelectedItem(AtRow: indexPath.row)
        self.performSegue(withIdentifier: Segue.toDetail, sender: self)
    }

}

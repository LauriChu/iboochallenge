//
//  Photo.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation
import SwiftyJSON

class PhotoJSON {
    
    var id: String
    var owner: String
    var secret: String
    var server: String
    var farm: Int
    var title: String
    var ispublic: Int
    var isfriend: Int
    var isfamily: Int
    
    init(json: JSON){
        id = json["id"].string!
        owner = json["owner"].string!
        secret = json["secret"].string!
        server = json["server"].string!
        farm = json["farm"].int!
        title = json["title"].string!
        ispublic = json["ispublic"].int!
        isfriend = json["isfriend"].int!
        isfamily = json["isfamily"].int!
    }
}

    

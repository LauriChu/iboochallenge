//
//  DetailViewController.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {
    
    // MARK: Properties
    
    var itemCollection: CollectionItem!

    @IBOutlet weak var bigImageView: UIImageView!
    @IBOutlet weak var mSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add Image and State of CollectionItem
        CellManager.inflateSDWebImage(image: bigImageView, url: itemCollection.bigImageUrl)
        mSwitch.isOn = itemCollection.isEnabled
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Detects when switch changes
    @IBAction func onSwitchChanged(_ sender: UISwitch) {
        itemCollection.isEnabled = sender.isOn
    }
}

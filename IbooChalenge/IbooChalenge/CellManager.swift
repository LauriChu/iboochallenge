//
//  CellManager.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class CellManager {
    
    public static func inflateSDWebImage(image: UIImageView, url: String){
        image.sd_setShowActivityIndicatorView(true)
        image.sd_setIndicatorStyle(.gray)
        image.sd_setImage(with: URL(string: url))
    }
    
    enum CellId: String {
        case collection = "collectionCell"
        case load = "loadCell"
    }
}

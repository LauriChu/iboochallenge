//
//  Collection.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation

class CollectionItem {
    var smallImageUrl: String!
    var bigImageUrl: String!
    var isEnabled: Bool!
    
    init(smallImageUrl: String, bigImageUrl: String, enabled: Bool) {
        self.smallImageUrl = smallImageUrl
        self.bigImageUrl = bigImageUrl
        self.isEnabled = enabled
    }
}

//
//  FlickerResponse.swift
//  IbooChalenge
//
//  Created by Laura Garcia Fernandez on 14/2/17.
//  Copyright © 2017 Laura Garcia Fernandez. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlickrJSON {
    var photos: ContentJSON
    var stat: String
    
    init(json: JSON){
        photos = ContentJSON(json: json["photos"])
        stat = json["stat"].string!
    }
    
}
